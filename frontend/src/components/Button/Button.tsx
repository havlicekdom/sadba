import React, { ReactNode } from 'react';

interface Props {
  variant: 'default' | 'primary' | 'link' | 'success' | 'error' ;
  size?: 'sm' | 'lg';
  className?: string;
  children: ReactNode;
}

function Button(props: Props) {
  const className = `btn btn-${props.variant} ${props.size ? `btn-${props.size}` : ''} ${props.className || ''}`;

  return (
    <button className={className}>
      { props.children }
    </button>
  );
}

export default Button;