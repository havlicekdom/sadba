import React, { ReactNode } from 'react';
import { Link } from 'react-router-dom';
import './Modal.scss';

interface Props {
  closeUrl: string;
  content: ReactNode;
  footer: ReactNode;
  title: string;
  size?: 'sm' | 'lg';
}

function Modal(props: Props) {
  const {
    closeUrl,
    content,
    footer,
    title,
    size,
  } = props;

  return (
    <div className={`modal active ${size ? `modal-${size}` : ''}`}>
      <Link to={closeUrl} className="modal-overlay" aria-label="Close"></Link>
      <div className="modal-container">
        <div className="modal-header">
          <Link to={closeUrl} className="btn btn-clear float-right" aria-label="Close"></Link>
          <div className="modal-title h5">
            { title }
          </div>
        </div>
        <div className="modal-body">
          <div className="content">
            { content }
          </div>
        </div>
        <div className="modal-footer">
          { footer }
        </div>
      </div>
    </div>
  );
}

export default Modal;
