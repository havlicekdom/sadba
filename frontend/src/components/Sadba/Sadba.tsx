import React, { useEffect } from 'react';
import { RouteComponentProps } from 'react-router';
import { Route } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { fetchList } from './../../store/modules/sadba/thunks';
import { selectList } from './../../store/modules/sadba/selectors';
import './Sadba.scss';
import SadbaList from './SadbaList';
import SadbaEdit from './SadbaEdit';

interface Props extends RouteComponentProps {}

function Sadba(props: Props) {
  const { match } = props;
  const dispatch = useDispatch();
  const list = useSelector(selectList);

  useEffect(() => {
    dispatch(fetchList());
  }, [dispatch]);

  return (
    <>
      <div className="row">
        <div className="column col-6 col-lg-12 col-mx-auto">
          <div className="panel">
            <div className="panel-header text-center">
              <div className="panel-title h5">Sadba</div>
            </div>
            <div className="panel-body">
              <SadbaList list={list} />
            </div>
            <div className="panel-footer">
              <button className="btn btn-primary float-right">
                <i className="icon icon-plus"></i> Přidat nový záznam
              </button>
            </div>
          </div>
        </div>
      </div>
      <Route path={`${match.url}/edit/:sadbaId`} component={SadbaEdit} />
    </>
  );
}

export default Sadba;