import React from 'react';
import { Link } from 'react-router-dom';
import Button from '../../../Button';

interface Props {
  closeUrl: string;
}

function Footer(props: Props) {
  const { closeUrl } = props;
  
  return (
    <div className="btn-group">
      <Button variant="primary">
        Uložit
      </Button>
      <Link to={closeUrl} className="btn">
        Zavřít
      </Link>
    </div>
  );
}

export default Footer;