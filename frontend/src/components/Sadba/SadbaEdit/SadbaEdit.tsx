import React from 'react';
import Modal from '../../Modal';
import Content from './Content';
import Footer from './Footer';

function SadbaEdit() {
  const closeUrl = "/sadba";

  return (
    <Modal
      closeUrl={closeUrl}
      title="Editace sadby"
      content={ <Content /> }
      footer={ <Footer closeUrl={closeUrl} /> }
    />
  );
}

export default SadbaEdit;