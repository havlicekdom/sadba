import React from 'react';
import { Sadba } from '../../../store/modules/sadba/types';
import SadbaItem from '../SadbaItem';

interface Props {
  list?: Sadba[] | null;
}

function SadbaList(props: Props) {
  const listItems = props.list?.map((item) => {
    return (
      <SadbaItem key={item._id} item={item} />
    );
  });

  return (
    <>
      { listItems }
    </>
  );
}

export default SadbaList;