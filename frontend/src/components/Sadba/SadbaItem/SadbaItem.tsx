import React from 'react';
import { Link } from 'react-router-dom';
import { Sadba } from './../../../store/modules/sadba/types';
import { formatLocation } from '../../../utils/string';
import './SadbaItem.scss';

interface Props {
  item: Sadba;
}

function SadbaItem(props: Props) {
  const { item } = props;

  return (
    <div className="tile">
      <div className="tile-content">
        <div className="tile-title text-bold mb-2">
          { item.name }
          <div className="btn-group float-right">
            <Link to={`/sadba/edit/${item._id}`} className="btn btn-primary">
              <i className="icon icon-edit"></i>
            </Link>
            <button className="btn btn-primary">
              <i className="icon icon-delete"></i>
            </button>
          </div> 
        </div>
        <div className="tile-subtitle">
          <div>
            <b>Lokace:</b> { formatLocation(item.location) }
          </div>
          <div>
            <b>Počet:</b> { item.count }
          </div>
        </div>
      </div>
    </div>
  );
}

export default SadbaItem;