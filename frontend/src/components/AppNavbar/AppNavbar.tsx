import React from 'react';
import { NavLink } from 'react-router-dom';
import './AppNavbar.scss';

function AppNavbar() {
  return (
    <header className="navbar bg-primary app-navbar">
      <div className="container size-xl">
        <div className="row">
          <div className="column col-12 d-flex">
            <section className="navbar-section">
              <NavLink to="/" exact className="navbar-brand">
                <b>Sadba</b>Tracker
              </NavLink>
            </section>
            <section className="navbar-section">
              <NavLink to="/" exact className="nav-item">
                Home
              </NavLink>
              <NavLink to="/sadba" exact className="nav-item">
                Sadba
              </NavLink>
              <NavLink to="/odruda" exact className="nav-item">
                Odrůdy
              </NavLink>
            </section>
          </div>
        </div>
      </div>
    </header>
  );
}

export default AppNavbar;