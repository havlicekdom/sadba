import { SadbaLocation } from "../store/types";

export function formatLocation(location: SadbaLocation): string {
  return SadbaLocation[location];
}