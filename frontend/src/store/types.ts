import { SadbaState } from "./modules/sadba/types";

export enum SadbaLocation {
  Skleník = 1,
  Obchod = 2,
}

export interface RootState {
  sadba: SadbaState;
}