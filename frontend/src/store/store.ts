import { configureStore } from '@reduxjs/toolkit';
import sadbaReducer from './modules/sadba';

export default configureStore({
  reducer: {
    sadba: sadbaReducer,
  },
});