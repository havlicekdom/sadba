import { createSlice } from '@reduxjs/toolkit';
import sadbaReducer, { initialState } from './reducer';

const sadbaSlice = createSlice({
  name: 'sadba',
  initialState,
  reducers: {},
  extraReducers: sadbaReducer,
});

export default sadbaSlice.reducer;