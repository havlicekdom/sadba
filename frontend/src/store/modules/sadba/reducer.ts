import { PayloadAction, ActionReducerMapBuilder, SerializedError } from '@reduxjs/toolkit';
import { SadbaState, Sadba } from './types';
import { fetchList } from './thunks';

export const initialState: SadbaState = {
  list: null,
  loading: false,
  error: null,
};

const reducer = (builder: ActionReducerMapBuilder<SadbaState>) => {
  builder.addCase(fetchList.pending, (state: SadbaState) => {
    state.loading = true;
  });
  builder.addCase(fetchList.fulfilled, (state: SadbaState, action: PayloadAction<Sadba[]>) => {
    state.loading = false;
    state.list = action.payload;
    state.error = null;
  });
  builder.addCase(fetchList.rejected, (state: SadbaState, action: PayloadAction<unknown, string, unknown, SerializedError>) => {
    state.loading = false;
    state.error = action.error.message ?? '';
  });
};

export default reducer;