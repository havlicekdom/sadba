import { createSelector } from 'reselect';
import { RootState } from '../../types';

const getSadbaState = (state: RootState) => state.sadba;

export const selectList = createSelector(
  [getSadbaState],
  (state) => state.list,
);