import { SadbaLocation } from '../../types';

export interface Sadba {
  odruda: number;
  location: SadbaLocation;
  count: number;
  _id: number;
  created: number;
  name: string;
}

export interface SadbaState {
  list: Sadba[] | null;
  loading: boolean;
  error: string | null;
}