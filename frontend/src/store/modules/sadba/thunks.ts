import { createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import apiRoutes from '../../apiRoutes';

export const fetchList = createAsyncThunk('fetchList', async() => {
  const response = await axios.get(apiRoutes.sadba.getAll);

  return response.data;
});