import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import 'spectre.css/src/spectre.scss';
import 'spectre.css/src/spectre-icons.scss';
import App from './App';
import * as serviceWorker from './serviceWorker';
import configuredStore from './store/store';

ReactDOM.render(
  <React.StrictMode>
    <Provider store={configuredStore}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
