import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import Home from './components/Home';
import Odruda from './components/Odruda';
import Sadba from './components/Sadba';

function App() {
  return (
    <Router>
      <AppNavbar />
      <div className="container size-xl">
        <div className="row">
          <div className="column col-12">
            <Switch>
              <Route path="/odruda" component={Odruda} />
              <Route path="/sadba" component={Sadba} />
              <Route path="/" exact component={Home} />
            </Switch>
          </div>
        </div>
      </div>
    </Router>
  );
}

export default App;
