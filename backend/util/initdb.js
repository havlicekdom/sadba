const monk = require('monk');
const db = monk('localhost:27017/sadba');

db.then(() => {
  console.log('Database created. Creating collections now.');
  db.create('sadba');
  db.create('counters');
  db.create('vysev');
  db.create('odruda');
  console.log('Collections created. Closing connection.');
}).catch((e) => {
  console.error(e);
});

process.exit();
