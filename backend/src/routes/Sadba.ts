import {
  Request,
  Response,
  Router,
} from 'express';
import { OK, CREATED, NO_CONTENT } from 'http-status-codes';
import SadbaDao from '@daos/SadbaDao';
import { sendError } from '@shared/functions';

const router = Router();
const dao = new SadbaDao();

router.get('/all', async (req: Request, res: Response) => {
  let data;

  try {
    data = await dao.getAll();
  } catch (e) {
    sendError(res, e);
  }

  res.status(OK).json(data);
});

router.get('/:id', async (req: Request, res: Response) => {
  let data;

  try {
    data = await dao.getOne(req.params.id);
  } catch (e) {
    sendError(res, e);
  }

  res.status(OK).json(data);
});

router.post('/new', async (req: Request, res: Response) => {
  try {
    await dao.createOne(req.body);
  } catch (e) {
    sendError(res, e);
  }

  res.status(CREATED).send();
});

router.put('/:id', async (req: Request, res: Response) => {
  try {
    await dao.updateOne(req.params.id, req.body);
  } catch (e) {
    sendError(res, e);
  }

  res.status(NO_CONTENT).send();
});

router.delete('/:id', async (req: Request, res: Response) => {
  try {
    await dao.deleteOne(req.params.id);
  } catch (e) {
    sendError(res, e);
  }

  res.status(NO_CONTENT).send();
});

export default router;
