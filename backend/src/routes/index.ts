import { Router } from 'express';
import OdrudaRouter from './Odruda';
import SadbaRouter from './Sadba';

// Init router and path
const router = Router();

router.use('/odruda', OdrudaRouter);
router.use('/sadba', SadbaRouter);

// Export the base-router
export default router;
