export default async function (array: any[], callback: (item: any, index: number, array: any[]) => Promise<any>) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
};
