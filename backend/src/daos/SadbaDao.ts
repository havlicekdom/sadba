import Database from '@db/Database';
import IDbSadba, { IOutputSadba, IInputSadba } from '@entities/Sadba';
import OdrudaDao from '@daos/OdrudaDao';
import asyncForEach from '../utils/asyncForEach';

class SadbaDao {
  private db: Database;
  private table: string = 'sadba';
  private odrudaDao: OdrudaDao;

  constructor() {
    this.db = new Database();
    this.odrudaDao = new OdrudaDao();
  }

  public async getAll(): Promise<IOutputSadba[]> {
    let data;

    try {
      data = await this.db.selectAll(this.table);
      data = await this.joinOdrudaTable(data);
    } catch (e) {
      throw e;
    }

    return data;
  }

  public async getOne(id: string): Promise<IOutputSadba> {
    let data;

    try {
      data = await this.db.selectOne(this.table, id);
    } catch (e) {
      throw e;
    }

    return data;
  }

  public async createOne(data: IInputSadba): Promise<void> {
    try {
      await this.db.createOne(this.table, data);
    } catch (e) {
      throw e;
    }
  }

  public async updateOne(id: string, data: IInputSadba): Promise<void> {
    try {
      await this.db.updateOne(this.table, id, data);
    } catch (e) {
      throw e;
    }
  }

  public async deleteOne(id: string): Promise<void> {
    try {
      await this.db.deleteOne(this.table, id);
    } catch (e) {
      throw e;
    }
  }

  private async joinOdrudaTable(data: IDbSadba[]): Promise<IOutputSadba[]> {
    await asyncForEach(data, async (item: IDbSadba) => {
      let odruda;

      try {
        odruda = await this.odrudaDao.getOne(item.odruda.toString());
      } catch (e) {
        throw e;
      }

      (item as IOutputSadba).name = odruda.name;
    });

    return data as IOutputSadba[];
  }
}

export default SadbaDao;
