import Database from '@db/Database';
import IOdruda from '@entities/Odruda';

class OdrudaDao {
  private db: Database;
  private table: string = 'odruda';

  constructor() {
    this.db = new Database();
  }

  public async getAll(): Promise<IOdruda[]> {
    let data;

    try {
      data = await this.db.selectAll(this.table);
    } catch (e) {
      throw e;
    }

    return data;
  }

  public async getOne(id: string): Promise<IOdruda> {
    let data;

    try {
      data = await this.db.selectOne(this.table, id);
    } catch (e) {
      throw e;
    }

    return data;
  }

  public async createOne(data: IOdruda): Promise<void> {
    try {
      await this.db.createOne(this.table, data);
    } catch (e) {
      throw e;
    }
  }

  public async updateOne(id: string, data: IOdruda): Promise<void> {
    try {
      await this.db.updateOne(this.table, id, data);
    } catch (e) {
      throw e;
    }
  }

  public async deleteOne(id: string): Promise<void> {
    try {
      await this.db.deleteOne(this.table, id);
    } catch (e) {
      throw e;
    }
  }
}

export default OdrudaDao;
