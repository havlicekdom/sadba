import logger from './Logger';
import { Response } from 'express';
import { BAD_REQUEST } from 'http-status-codes';

export const pErr = (err: Error) => {
    if (err) {
        logger.error(err);
    }
};

export const getRandomInt = () => {
    return Math.floor(Math.random() * 1_000_000_000_000);
};

export const sendError = (res: Response, err: Error) => {
  pErr(err);
  res.status(BAD_REQUEST).send();
};
