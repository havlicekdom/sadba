import monk, { IMonkManager } from 'monk';

interface IDbData {
  [key: string]: any;
}

class Database {
  private db: IMonkManager;

  constructor() {
    this.db = monk('localhost:27017/sadba');
  }

  public async selectAll(table: string): Promise<any> {
    const collection = this.db.get(table);
    let result;

    try {
      result = await collection.find({});
    } catch (e) {
      throw e;
    }

    return result;
  }

  public async selectOne(table: string, id: string): Promise<any> {
    const collection = this.db.get(table);
    let result;

    try {
      result = await collection.findOne({
        _id: parseInt(id, 10),
      });
    } catch (e) {
      throw e;
    }

    return result;
  }

  public async createOne(table: string, data: IDbData): Promise<any> {
    const collection = this.db.get(table);
    let result;
    try {
      const nextId = await this.getNextSequenceValue(table);
      data._id = nextId.count;
      data.created = Date.now();
      result = await collection.insert(data);
    } catch (e) {
      throw e;
    }

    return result;
  }

  public async updateOne(table: string, id: string, data: IDbData): Promise<any> {
    const collection = this.db.get(table);
    let result;

    try {
      result = await collection.update(
        { _id: parseInt(id, 10) },
        { $set: data },
      );
    } catch (e) {
      throw e;
    }

    return result;
  }

  public async deleteOne(table: string, id: string): Promise<any> {
    const collection = this.db.get(table);
    let result;
    try {
      result = collection.remove(
        { _id: parseInt(id, 10) },
      );
    } catch (e) {
      throw e;
    }

    return result;
  }

  private async getNextSequenceValue(name: string): Promise<any> {
    const collection = this.db.get('counters');
    let result;

    try {
      result = await collection.findOneAndUpdate(
        {
          name,
        },
        {
          $inc: {
            count: 1,
          },
        },
      );
    } catch (e) {
      throw e;
    }

    return result;
  }
}

export default Database;
