export enum SadbaLocation {
  Greenhouse = 1,
  Store = 2,
}

export interface IInputSadba {
  odruda: number;
  location: SadbaLocation;
  count: number;
}

export default interface IDbSadba extends IInputSadba {
  _id: number;
  created: number;
}

export interface IOutputSadba extends IDbSadba {
  name: string;
}
